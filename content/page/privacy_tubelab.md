## Application privacy-policy

### This page is dedicated to the TubeLab privacy-policy


#### Stored data
Only basic information from connected accounts are stored locally on the device. These data remain strictly confidential and are only usable by the application. Removing the application leads to the deletion of these data.

No logins and passwords are stored locally. They are only used to get the access token. This token can be revoked at any time with Peertube.

All connections are proceed through SSL encryptions to an instance. The application will not connect to an instance that presents a non valid certificate.

#### Authorization with the API
  - `Read` : Read data from the account
  - `Write` : Write messages
  - `Follow` : Follow, unfollow, block mute accounts

These actions are performed only by the user.

#### Tracking and Advertisements
The application does not use any tracking tools and does not run advertising.
